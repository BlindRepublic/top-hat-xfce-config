# Directories
xml_DIR		= xfce4/xfconf/xfce-perchannel-xml
profile_DIR	= xfce4-panel-profiles
panel_DIR	= xfce4/panel
auto_DIR	= autostart
dot_DIR		= .

# Files
# Globs
xml		= $(wildcard $(xml_DIR)/*)
panel_profiles	= $(wildcard $(profile_DIR)/*)
panel_extra	= $(wildcard $(panel_DIR)/*)
autostart	= $(wildcard $(auto_DIR)/*)
dotfiles	= $(wildcard $(dot_DIR)/DOT*)

# One-offs
#helpers		= xfce4/helpers.rc
catfish 	= catfish.rc
terminal	= terminalrc

# Destinations
skel_DEST	= /etc/skel
share_DEST 	= $(DESTDIR)/$(PREFIX)/share
config_DEST	= $(DESTDIR)/$(skel_DEST)/.config
xfce_DEST	= $(config_DEST)/xfce4
xml_DEST	= $(config_DEST)/$(xml_DIR)
panel_DEST	= $(config_DEST)/$(panel_DIR)
profile_DEST	= $(share_DEST)/xfce4-panel-profiles/layouts
auto_DEST	= $(config_DEST)/$(auto_DIR)
dot_DEST	= $(skel_DEST)/$(dot_DIR)
catfish_DEST	= $(config_DEST)/catfish/
terminal_DEST	= $(xfce_DEST)/terminal

ifeq ($(PREFIX),)
	PREFIX	:= /usr/local
endif

# RPM stuff

default:
	$(error This makefile is just for installation/uninstallation for now!)

install:
	# XML 
	install -d $(xml_DEST)
	for x in $(xml); do \
		install -m644 $$x $(xml_DEST); \
	done
	# Panel profiles
	install -d $(profile_DEST)
	for p in $(panel_profiles); do \
		install -m644 "$$p" $(profile_DEST); \
	done
	# Extra panel configuration files
	install -d $(panel_DEST)
	for p in $(panel_extra); do \
		install -m644 "$$p" $(panel_DEST); \
	done
	# Autostart files
	install -d $(auto_DEST)
	for a in $(autostart); do \
		install -m644 "$$a" $(auto_DEST); \
	done
	# Dotfiles
	install -d $(dot_DEST)
	# Extra files
	#install -m644 $(helpers) $(xfce_DEST)
	install -d $(catfish_DEST)
	install -m644 $(catfish) $(catfish_DEST)
	install -d $(terminal_DEST)
	install -m644 $(terminal) $(terminal_DEST)
uninstall: 
	rm -r $(xfce_DEST)
	rm -r $(profile_DEST)
	rm -r $(auto_DEST)
	rm -r $(catfish_DEST)
	rm -r $(terminal_DEST)

